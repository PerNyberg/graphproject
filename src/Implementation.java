import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.Minutes;
import org.joda.time.Years;


public class Implementation {
	
	public static void main(String[] args){
		TrafficNetwork<Node> network = new TrafficNetwork<Node>();
		
		//--------NODES---------------//
		Node kista = new Node("Kista",1,2);
		Node stockholm = new Node("Stockholm",2,3);
		Node t�by = new Node("T�by",2,3);
		Node danderyd = new Node("Danderyd",2,3);
		Node ormsta = new Node("Ormsta",2,3);
		
		network.addNode(kista);
		network.addNode(stockholm);
		network.addNode(t�by);
		network.addNode(danderyd);
		network.addNode(ormsta);
		//--------------------------//
		
		// ---------DEPARTURES--------------//
		//------------ Kista bus 1 --------------------//
		ArrayList<DateTime> departures1 = new ArrayList<DateTime>();
		DateTime ett = new DateTime(2015,3,13,21,0);
		departures1.add(ett);

		//------------ DEP 2 --------------------//
		ArrayList<DateTime> departures2 = new ArrayList<DateTime>();
		DateTime fyra = new DateTime(2015,3,13,21,40);
		departures2.add(fyra);

		//------------ DEP 3 --------------------//
		ArrayList<DateTime> departures3 = new ArrayList<DateTime>();
		DateTime �tta = new DateTime(2015,3,13,22,0);
		departures3.add(�tta);

		//------------ DEP 4 --------------------//
		ArrayList<DateTime> departures4 = new ArrayList<DateTime>();
		DateTime tolv = new DateTime(2015,3,13,22,40);
		departures4.add(tolv);
		//------------ DEP 5 --------------------//
		ArrayList<DateTime> departures5 = new ArrayList<DateTime>();
		DateTime tretton = new DateTime(2015,3,13,19,00);
		departures5.add(tretton);
		//---------CONNECTIONS-------------//

		
		network.connect(kista,danderyd,"Bus 2",10,departures3);
		network.connect(danderyd,stockholm,"Bus 2",10,departures4);
		
		network.connect(kista,stockholm, "Bus 3",5, departures5);
		
		network.connect(stockholm,danderyd,"Bus 1",10,departures1);
		network.connect(danderyd,kista,"Bus 1",10,departures2);

		
		// F�r inte vara datum som redan passerat
		// Alla noder m�ste ha n�gon line
		// Alla noder kr�ver att det finns kommande resor. Kanske b�ttre med annat system f�r departures?
		//----------Tests--------------//

		//network.getRoute(stockholm,kista);
		network.getRoute(kista,stockholm);


		
	}
}
