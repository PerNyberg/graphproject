import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.Minutes;
import org.joda.time.MutableDateTime;

/**
 * Class to represent a connection in a public transportation system. The connection can be a particular bus, train or subway line.
 * 
 * @author peny6059
 *
 */

public class Line<N>{
	private N dest;
	private N origin;
	private String name;
	private int weight;
	private LinkedList<DateTime> departures;
	
	/**
	 * Constructs a line with an origin, destination, name, weight and departures
	 *
	 * @param origin 	the node at which this line originates
	 * @param dest		the node which is the destination of this line
	 * @param name		the name of this line
	 * @param weight	the time it takes to get from the origin to the destination
	 * @param departures	a list of departure time for this line
	 */
	Line(N origin, N dest, String name, int weight, List<DateTime> departures){
		this.origin = origin;
		this.dest = dest;
		this.name = name;
		this.weight = weight;
		this.departures = new LinkedList<DateTime>();
		for(DateTime e : departures){
			this.departures.add(e);
		}
	}
	/**
	 * Adds a new departure to this line
	 *
	 * @param  departure  		a DateTime object with the exact time of the departure
	 * @return      			void
	 */
	public void addDeparture(DateTime departure){		// Adds a departure time of the format hh:mm
		departures.add(departure);
	}
	/**
	 * Removes a departure from a specific line from a node. 
	 *
	 * @param  departure  		a DateTime object with the departure to remove
	 * @return      			void
	 */
	public void removeDeparture(DateTime departure){
		if(departures.contains(departure)){
			departures.remove(departure);
		}else{
			System.out.println("Departure did not exist");
		}
	}
	public N getDest(){
		return dest;
	}
	public N getOrigin(){
		return origin;
	}
	public String getName(){
		return name;
	}
	/**
	 * Returns the weight of this line + the time until the next departure. 
	 *
	 * @param  startDate  			a DateTime object which represents the users current time so that the time until the next departure can be found
	 * @return weight+departure     returns the weight of this line + the time until the next departure. 
	 */
	public int getWeight(MutableDateTime startDate){		// Returns the weight + the time until the next departure. startDate is the time the person can start travelling from this location.
		int untilDeparture = Integer.MAX_VALUE;
		for(DateTime departure : departures){		// Getting the time until departure that is closest to the startDate
			if(Minutes.minutesBetween(startDate,departure).getMinutes() < untilDeparture && Minutes.minutesBetween(startDate,departure).getMinutes() > 0){
				untilDeparture = Minutes.minutesBetween(startDate,departure).getMinutes();
			}
		}
		return weight+untilDeparture;
	}
	public int getIdealWeight(){		// returns weight independently of departures
		return weight;
	}
	public void setWeight(int weight){
		if(weight < 0){
			throw new IllegalArgumentException();
		}else{
			this.weight = weight;
		}
	}
	public String toString(){
		return name+" to " + dest;
	}
}