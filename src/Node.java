
/**
 * Class to represent a node in a public transportation system. The node can be a particular bus, train or subway station.
 * 
 * @author peny6059
 *
 */


class Node{
	private String name;
	private double lati;
	private double longi;

	public Node(String name, double lati, double longi){
		this.lati = lati;
		this.longi = longi;
		this.name = name;
	}
	public String getName(){
		return name;
	}
	public String toString(){
		return name;
	}
}

