/*
 * Class to represent a graph of a public transportation system. The graph is built of Node and Line-objects.
 * data: 		A hashmap that links nodes together to their lines.
 */

import java.text.DecimalFormat;
import java.util.*;

import org.joda.time.DateTime;
import org.joda.time.MutableDateTime;

/**
 * Class to represent a network of a public transportation system. Uses node and line objects.
 * 
 * @author peny6059
 *
 */

public class TrafficNetwork<N>{
	private Map<N, List<Line<N>>> data = new HashMap<N, List<Line<N>> >();

	/**
	 * Adds a new departure to a specific line from a node. 
	 *
	 * @param  newDeparture  	a DateTime object with the exact time of the departure
	 * @param  lineName 		the name of the line which to add the departure to
	 * @param  node 			the node from where the line starts			
	 * @return      			void
	 */
	public void addLineDeparture(DateTime newDeparture, String lineName, N node){		// Adds another departure for an already defined line
		for(Line<N> line : data.get(node)){
			if(line.getName().equals(lineName)){
				line.addDeparture(newDeparture);
			}
		}
	}
	/**
	 * Removes a departure from a specific line from a node. 
	 *
	 * @param  departure  		a DateTime object with the departure to remove
	 * @param  lineName 		the name of the line which to remove the departure from
	 * @param  node 			the node from where the line starts
	 * @return      			void
	 */
	public void removeLineDeparture(DateTime departure, String lineName, N node){		// Removes a departure from an already defined line
		for(Line<N> line : data.get(node)){
			if(line.getName().equals(lineName)){
				line.removeDeparture(departure);
			}
		}
	}
	/**
	 * Adds a new node to the graph.
	 *
	 * @param  newDeparture  	a DateTime object with the exact time of the departure
	 * @param  lineName 		the name of the line which to add the departure to
	 * @param  node 			the node from where the line starts
	 * @return      			void
	 */
	public void addNode(N newNode){						// Creates new node
		if (!data.containsKey(newNode)){
			data.put(newNode, new ArrayList<Line<N>>());
		}
	}
	/**
	 * Adds a one way connection between two nodes in the graph. Returns a boolean.
	 *
	 * @param  from  		a node object from where the new line will start
	 * @param  to 			the node that will be the new lines destination
	 * @param  lineName 	the name of the new line
	 * @param  weight		the weight of the new line
	 * @param  departures	a list of DateTime objects that will be the departure times for this line
	 * @return      		true if connection could be made, false otherwise
	 */
	public boolean connect(N from, N to, String lineName, int weight, List<DateTime> departures){		// Connects two nodes with line, adding departure times should be done separately with the method addLineDeparture
		
		DateTime current = new DateTime();
		for(DateTime dt : departures){
			if(dt.isBefore(current)){
				System.out.println("Couldn't connect nodes as departures contained a passed date");
				return false;
			}
		}
		
		if(!data.containsKey(from) || !data.containsKey(to)){
			throw new NoSuchElementException("Node does not exist");
		}else if(weight < 0){
			throw new IllegalArgumentException("Weight can't be negative");
		}else if(getLineBetween(from,to,lineName) != null){
			throw new IllegalStateException("There is already a connection between these nodes");
		}else{
			data.get(from).add(new Line<N>(from,to,lineName,weight,departures));
			return true;
		}
	}
	/** ??????????????????????????? to?
	 * Sets the weight of a line in the graph.
	 *
	 * @param  from  		a node object from where the line starts
	 * @param  to 			the destination of the line
	 * @param  lineName 	the name of the line whose weight is being changed
	 * @param  weight		the new weight of the line
	 * @return      		void
	 */
	public void setConnectionWeight(N from, N to, int weight,String lineName){			// Sets the weight of a particular line
		if(weight < 0){
			throw new IllegalArgumentException("Weight can't be negative");
		}else if(!data.containsKey(from) || !data.containsKey(to)){
			throw new NoSuchElementException("Node does not exist in graph");
		}else if(getLineBetween(from,to,lineName) == null){
			throw new NoSuchElementException("These nodes do not have a connection");
		}else{
			getLineBetween(from,to,lineName).setWeight(weight);
		}
	}
	/**
	 * Returns a list of all the nodes in the graph.
	 *
	 * @return      		a list of all the nodes in the graph
	 */
	public List<N> getNodes(){		// Returns a list of all the nodes in the network
		return new ArrayList<N> (data.keySet());
	}
	/**
	 * Returns a list of all the lines from a node in the graph.
	 *
	 * @param  where  		the node whose connections the method will return
	 * @return      		a list of all the lines from the node
	 */
	public List<Line<N>> getLinesFrom(N where){		// Returns a list of all the lines from a node
		if (!data.containsKey(where)){
			throw new NoSuchElementException("Node does not exist in graph");
		}else{
			return new ArrayList<Line<N>>(data.get(where));
		}
	}
	/** ??????????????????????????? to?
	 * Returns a line between two nodes with the specified lineName.
	 *
	 * @param  from  		a node object from where the line starts
	 * @param  to 			the destination of the line
	 * @param  lineName 	the name of the line to find
	 * @return      		a line-object with the specificed lineName
	 */
	public Line<N> getLineBetween(N from, N to, String lineName){		// Returns the line between two nodes. ??? Shouldn't there be the possibility of several lines between the same stations?
		if(!data.containsKey(from) || !data.containsKey(to)){
			throw new NoSuchElementException("Node does not exist in graph");
		}else{
			try{
				for(Line<N> e : data.get(from)){
					if (e.getDest().equals(to) && e.getName().equals(lineName)){
						return e;
					}
				}
			}catch(NullPointerException e){
				return null;
			}
		}
		return null;
	}
	/**
	 * Removes a node from the graph and the connections to and from it.
	 *
	 * @param  node  		the node object to remove
	 * @return      		void
	 */
	public void remove(N node){		// Removes a node and connections to and from it
		if(data.containsKey(node)){
			List<Line<N>> lines = getLinesFrom(node);
			for(Line<N> n : lines){
				disconnect(node,n.getDest(),n.getName());
			}
			data.remove(node);
		}
	}
	/** ???????????????????????????? to??
	 * Removes a line from a node with the name that matches the lineName parameter.
	 *
	 * @param  from  		the node object from where the line starts
	 * @param  to  			the node object which is the lines destination
	 * @param  lineName  	the name of the line to be removed
	 * @return      		void
	 */
	public void disconnect(N from, N to, String lineName){ // Removes the connection between two nodes.
		if(data.containsKey(from) && data.containsKey(to)){
			Line<N> toDelete = null;
			
			for(Line<N> e : data.get(from)){
				if(e.getName().equals(lineName)){
					toDelete = e;
				}
			}
			data.get(from).remove(toDelete);
		}else{
			throw new NoSuchElementException("Node does not exist in graph");
		}
	}
	/**
	 * Returns a String representation of the graph.
	 *
	 * @return      		String that contains the graph
	 */
	public String toString(){
		String str = "";
		for(Map.Entry<N, List<Line<N>>> me : data.entrySet()){
			N staden = me.getKey();
			List<Line<N>> b�gar = me.getValue();
			str += staden.toString() + " : " + b�gar + "\n";
		}
		return str;
	}
	
	// ------------------------- Tog bort <N> fr�n metodsignaturen
	/**
	 * Returns a true if a path exists between two objects in the graph, and a false if not.
	 *
	 * @param  from  		the start
	 * @param  to  			the end
	 * @return      		boolean, true if path exists and false if path does not exist
	 */
	public boolean pathExists(N from,N to){			// Uses DFS to find if a path exists, does not take departures into account, can be wrong?
		if(getNodes().contains(from) || getNodes().contains(to)){
			Set<N> visited = new HashSet<N>();
			depthFirstSearch(from,visited);
			return visited.contains(to);
		}
		return false;
	}
	private void depthFirstSearch(N where, Set<N> visited){
		visited.add(where);
		for(Line<N> e : getLinesFrom(where)){
			if (! visited.contains(e.getDest())){
				depthFirstSearch(e.getDest(),visited);
			}	
		}
	}
	/**
	 * Returns a list of lines that represent the fastest way from node "from" to node "to".
	 * Otherwise returns a null.
	 * 
	 * Uses Dijkstra's algorithm.
	 *
	 * @param  from  		the start
	 * @param  to  			the end
	 * @return      		list of lines that gives the fastest way from from to to.
	 */
	public List<Line<N>> getRoute(N from, N to){			// Finds the quickest route between two nodes
		Map<N,Line<N>> via = new HashMap<N,Line<N>>();		// Via
		Map<N,Boolean> best = new HashMap<N,Boolean>();		// Undetermined nodes
		Map<N,Integer> tid = new HashMap<N,Integer>();		// Map with fastest times
		List<N> nodes = getNodes();
		List<Line<N>> route = new ArrayList<Line<N>>();
		boolean departureError = false;
		String routeInfo = "";
		
		DecimalFormat df = new DecimalFormat("00");					// Gives correct decimal format when printing results
		MutableDateTime currentTime = new MutableDateTime();		// Create new date object with the current time
		MutableDateTime nextDeparture2 = currentTime;

		if(pathExists(from,to) == true){
			for(N e : nodes){		// Fyll tabellerna
				via.put(e, null);
				best.put(e, false);
				tid.put(e,Integer.MAX_VALUE);
			}
			best.put(from,true);	// Set startnod to true
			tid.put(from,0);		// And its time to 0
			N currentNode = from;
			while(best.get(to) == false){
				currentTime = new MutableDateTime();
				for(Line<N> e : getLinesFrom(currentNode)){	// Iterate through the lines of the current node
					currentTime.addMinutes(tid.get(currentNode));
					if((tid.get(currentNode)+e.getWeight(currentTime)) < (tid.get(e.getDest()))){	// If a smaller time is found, save it
						tid.put(e.getDest(),tid.get(currentNode)+e.getWeight(currentTime));
						via.put(e.getDest(),e);	
					}
				}
				int minstatid = Integer.MAX_VALUE;
				for(N e : nodes){				// Finds the closest node that is not yet determined
					if ((tid.get(e) < minstatid) && (best.get(e) != true)){
						minstatid = tid.get(e);
						currentNode = e;
					}
				}
				best.put(currentNode,true);		// Set the new startnode to true
			}
			currentTime = new MutableDateTime();
			System.out.println("From "+from.toString()+" to "+to.toString()+" kl: "+df.format(currentTime.getHourOfDay())+":"+df.format(currentTime.getMinuteOfHour()));
			System.out.println("---------------------------");
			while(currentNode != from){
				Line<N> n = via.get(currentNode);
				route.add(n);
				currentNode = via.get(currentNode).getOrigin();
			}
			Collections.reverse(route);
			for(Line<N> e : route){
				nextDeparture2.addMinutes(e.getWeight(nextDeparture2)-e.getIdealWeight()+1);
				if(nextDeparture2.isBeforeNow()){
					departureError = true;
				}
				routeInfo += df.format(nextDeparture2.getHourOfDay())+":"+df.format(nextDeparture2.getMinuteOfHour())+" "+e.toString()+"\n";
			}
			
			if(!departureError){
				System.out.println(routeInfo);
			}else{
				System.out.println("Error with route");
			}
			
			return route;
		}else{
			return null;
		}
	}
	
}
